# Circles
Circles is no longer being maintained by FUTO.  You can find the latest version of Circles iOS at its [new home on Github](https://github.com/circles-project/circles-ios.git).